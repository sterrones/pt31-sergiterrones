package cat.inspedralbes.m8.mariobros.screens;

import java.util.concurrent.LinkedBlockingDeque;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.MarioGame;
import cat.inspedralbes.m8.mariobros.scenes.Hud;
import cat.inspedralbes.m8.mariobros.sprites.Enemy;
import cat.inspedralbes.m8.mariobros.sprites.Item;
import cat.inspedralbes.m8.mariobros.sprites.ItemDef;
import cat.inspedralbes.m8.mariobros.sprites.Mario;
import cat.inspedralbes.m8.mariobros.sprites.Mushroom;
import cat.inspedralbes.m8.mariobros.tools.Assets;
import cat.inspedralbes.m8.mariobros.tools.B2WorldCreator;
import cat.inspedralbes.m8.mariobros.tools.WorldContactListener;

public class PlayScreen implements Screen {
	// Referencia del juego
	private MarioGame game;
	private TextureAtlas atlas;

	private OrthographicCamera gameCam;
	public Viewport gamePort;

	// Hud
	private Hud hud;

	// Mapa
	private TmxMapLoader maploader;
	private TiledMap map;
	private OrthogonalTiledMapRenderer renderer;

	// Box2d variables
	private World world;
	private Box2DDebugRenderer b2dr;
	private B2WorldCreator creator;

	private Mario player;

	private Array<Item> items;
	private LinkedBlockingDeque<ItemDef> itemsToSpawn;

	public PlayScreen(MarioGame game, Assets assets) {

		atlas = new TextureAtlas("Mario_and_Enemies.pack");

		this.game = game;

		// Camara de seguimiento
		gameCam = new OrthographicCamera();

		// Mantener aspecto de medida en pantalla
		gamePort = new FitViewport(Constants.V_WIDTH / Constants.PPM, Constants.V_HEIGHT / Constants.PPM, gameCam);

		// Crear el hud
		hud = new Hud(game.batch, this);

		// Mapa
		maploader = new TmxMapLoader();
		map = maploader.load("Tiled/Level1/level1-1.tmx");
		renderer = new OrthogonalTiledMapRenderer(map, 1 / Constants.PPM);

		gameCam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

		// box2d
		world = new World(new Vector2(0, -10), true);
		b2dr = new Box2DDebugRenderer(true, true, false, true, false, true);

		// Bodies (comentar para ver todas las colisiones y cuerpos)
		b2dr.setDrawBodies(false);

		creator = new B2WorldCreator(this, assets);

		// Create player
		player = new Mario(this, assets);

		world.setContactListener(new WorldContactListener());

		assets.getMarioBackgroundSound().play();

		items = new Array<Item>();
		itemsToSpawn = new LinkedBlockingDeque<ItemDef>();

	}

	public void spawnItem(ItemDef idef) {
		itemsToSpawn.add(idef);
	}

	public void handleSpawningItems() {
		if (!itemsToSpawn.isEmpty()) {
			ItemDef idef = itemsToSpawn.poll();
			if (idef.type == Mushroom.class) {
				items.add(new Mushroom(this, idef.position.x, idef.position.y));
			}
		}
	}

	public TextureAtlas getAtlas() {
		return atlas;
	}

	@Override
	public void show() {

	}

	public void handleInput(float delta) {
		if (player.currentState != Mario.State.DEAD && player.currentState != Mario.State.WIN) {
			
//			if (Gdx.input.isKeyJustPressed(Input.Keys.UP) && player.currentState != Mario.State.FALLING
//					&& player.currentState != Mario.State.JUMPING) {
//				player.b2body.applyLinearImpulse(new Vector2(0, 3.5f), player.b2body.getWorldCenter(), true);
//			}
			
			if (Gdx.input.isKeyJustPressed(Input.Keys.UP) && player.b2body.getLinearVelocity().y == 0 ) {
				player.b2body.applyLinearImpulse(new Vector2(0, 3.5f), player.b2body.getWorldCenter(), true);
					}
			
			

			else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && player.b2body.getLinearVelocity().x <= 1.5) {
				player.b2body.applyLinearImpulse(new Vector2(5 * delta, 0), player.b2body.getWorldCenter(), true);

				if (player.currentState != Mario.State.DEAD)
					gameCam.position.x += 10 * delta;
			}

			else if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && player.b2body.getLinearVelocity().x >= -1.5) {
				player.b2body.applyLinearImpulse(new Vector2(-5 * delta, 0), player.b2body.getWorldCenter(), true);

				if (player.currentState != Mario.State.DEAD)
					gameCam.position.x -= 10 * delta;
			}
		}

		if (player.currentState == Mario.State.WIN) {
			player.b2body.applyLinearImpulse(new Vector2(5 * delta, 0), player.b2body.getWorldCenter(), true);
			removeObjects();
		}

	}

	public void removeObjects() {
		for (MapObject object : map.getLayers().get(7).getObjects()) {
			map.getLayers().get(7).getObjects().remove(object);
		}

	}

	public void update(float delta) {
		handleInput(delta);
		handleSpawningItems();

//		world.step(1/60f, 3, 2);		
		world.step(delta, 3, 2);

		player.update(delta);

		for (Enemy enemy : creator.getGoombas()) {
			enemy.update(delta);
			if (enemy.getX() < player.getX() + 224 / Constants.PPM)
				enemy.b2body.setActive(true);
		}

		for (Item item : items)
			item.update(delta);

		hud.update(delta);

		if (player.currentState != Mario.State.DEAD)
			gameCam.position.x = player.b2body.getPosition().x;

		gameCam.update();

		renderer.setView(gameCam);

	}

	@Override
	public void render(float delta) {
		update(delta);

		// Limpiar pantalla con negro
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Render mapa
		renderer.render();

		// Render Box2DDebugLines
		b2dr.render(world, gameCam.combined);

		// Dibuja

		// Player
		game.batch.setProjectionMatrix(gameCam.combined);
		game.batch.begin();

		player.draw(game.batch);

		for (Enemy enemy : creator.getGoombas())
			enemy.draw(game.batch);

		for (Item item : items)
			item.draw(game.batch);

		game.batch.end();

		// HUd
		game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
		hud.stage.draw();

		if (gameOver()) {
			game.setScreen(new GameOverScreen(game, hud));
			dispose();
		}

		if (gameWin()) {
			game.setScreen(new GameWinScreen(game, hud));
			dispose();
		}

	}

	public void timeOver() {
		player.currentState = Mario.State.DEAD;
	}

	public boolean gameOver() {
		if (player.currentState == Mario.State.DEAD && player.getStateTimer() > 3) {
			return true;
		}
		return false;
	}

	public boolean gameWin() {
		if (player.currentState == Mario.State.WIN && player.getStateTimer() > 3) {
			return true;
		}
		return false;
	}

	@Override
	public void resize(int width, int height) {
		gamePort.update(width, height);

	}

	public TiledMap getMap() {
		return map;
	}

	public World getWorld() {
		return world;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		map.dispose();
		renderer.dispose();
		world.dispose();
		b2dr.dispose();
		hud.dispose();

	}

}
