package cat.inspedralbes.m8.mariobros;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public class MarioGame extends Game {
	public SpriteBatch batch;
	public Assets assets;

	@Override
	public void create() {
		batch = new SpriteBatch();

		assets = new Assets();

		setScreen(new PlayScreen(this, assets));
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
		batch.dispose();
		assets.dispose();
	}
}
