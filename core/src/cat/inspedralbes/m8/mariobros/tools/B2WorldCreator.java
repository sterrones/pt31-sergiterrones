package cat.inspedralbes.m8.mariobros.tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.sprites.Brick;
import cat.inspedralbes.m8.mariobros.sprites.Coin;
import cat.inspedralbes.m8.mariobros.sprites.Goomba;

public class B2WorldCreator {

	private Array<Goomba> goombas;

	public B2WorldCreator(PlayScreen screen, Assets assets) {
		World world = screen.getWorld();
		TiledMap map = screen.getMap();

		BodyDef bdef = new BodyDef();
		PolygonShape shape = new PolygonShape();
		FixtureDef fdef = new FixtureDef();
		Body body;

		// Create ground bodies/fixtures
		for (MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			bdef.type = BodyDef.BodyType.StaticBody;
			bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,
					(rect.getY() + rect.getHeight() / 2) / Constants.PPM);

			body = world.createBody(bdef);

			shape.setAsBox(rect.getWidth() / 2 / Constants.PPM, rect.getHeight() / 2 / Constants.PPM);
			fdef.shape = shape;
			body.createFixture(fdef);
		}

		// Create pipes bodies/fixtures
		for (MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			bdef.type = BodyDef.BodyType.StaticBody;
			bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,
					(rect.getY() + rect.getHeight() / 2) / Constants.PPM);

			body = world.createBody(bdef);

			shape.setAsBox(rect.getWidth() / 2 / Constants.PPM, rect.getHeight() / 2 / Constants.PPM);
			fdef.shape = shape;
			fdef.filter.categoryBits = Constants.OBJECT_BIT;
			body.createFixture(fdef);
		}

		// Create bricks bodies/fixtures
		for (MapObject object : map.getLayers().get(5).getObjects().getByType(RectangleMapObject.class)) {

			new Brick(screen, object, assets);
		}

		// Create coins bodies/fixtures
		for (MapObject object : map.getLayers().get(4).getObjects().getByType(RectangleMapObject.class)) {

			new Coin(screen, object, assets);
		}

		// Create all goombas
		goombas = new Array<Goomba>();
		for (MapObject object : map.getLayers().get(6).getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			goombas.add(new Goomba(screen, rect.getX() / Constants.PPM, rect.getY() / Constants.PPM, assets));
		}

		// Create flag
		for (MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {

			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			bdef.type = BodyDef.BodyType.DynamicBody;
			bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PPM,
					(rect.getY() + rect.getHeight() / 2) / Constants.PPM);

			body = world.createBody(bdef);

			shape.setAsBox(rect.getWidth() / 2 / Constants.PPM, rect.getHeight() / 2 / Constants.PPM);
			fdef.shape = shape;
			fdef.filter.categoryBits = Constants.FLAG_BIT;
			body.createFixture(fdef);
		}

	}

	public Array<Goomba> getGoombas() {
		return goombas;
	}

}
