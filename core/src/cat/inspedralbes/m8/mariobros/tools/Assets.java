package cat.inspedralbes.m8.mariobros.tools;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class Assets {
	AssetManager manager;

	public Music marioBackgroundSound;
	public Sound coin;
	public Sound bump;
	public Sound breakBlock;
	public Sound powerUpSpawn;
	public Sound powerUp;
	public Sound powerDown;
	public Sound stomp;
	public Sound marioDie;
	public Sound marioFinish;

	public Assets() {
		manager = new AssetManager();
		manager.load("audio/music/mario_music.ogg", Music.class);
		manager.load("audio/sounds/coin.wav", Sound.class);
		manager.load("audio/sounds/bump.wav", Sound.class);
		manager.load("audio/sounds/breakblock.wav", Sound.class);
		manager.load("audio/sounds/powerup_spawn.wav", Sound.class);
		manager.load("audio/sounds/powerup.wav", Sound.class);
		manager.load("audio/sounds/powerdown.wav", Sound.class);
		manager.load("audio/sounds/stomp.wav", Sound.class);
		manager.load("audio/sounds/mariodie.wav", Sound.class);
		manager.load("audio/sounds/finish.wav", Sound.class);
		manager.finishLoading();

		loadAssets();
	}

	public void loadAssets() {
		marioBackgroundSound = manager.get("audio/music/mario_music.ogg", Music.class);
		marioBackgroundSound.setLooping(true);
		marioBackgroundSound.setVolume(0.3f);
		breakBlock = manager.get("audio/sounds/breakblock.wav", Sound.class);
		bump = manager.get("audio/sounds/bump.wav", Sound.class);
		coin = manager.get("audio/sounds/coin.wav", Sound.class);
		powerUpSpawn = manager.get("audio/sounds/powerup_spawn.wav", Sound.class);
		powerUp = manager.get("audio/sounds/powerup.wav", Sound.class);
		powerDown = manager.get("audio/sounds/powerdown.wav", Sound.class);
		stomp = manager.get("audio/sounds/stomp.wav", Sound.class);
		marioDie = manager.get("audio/sounds/mariodie.wav", Sound.class);
		marioFinish = manager.get("audio/sounds/finish.wav", Sound.class);
	}

	public AssetManager getManager() {
		return manager;
	}

	public Music getMarioBackgroundSound() {
		return marioBackgroundSound;
	}

	public Sound getCoin() {
		return coin;
	}

	public Sound getBump() {
		return bump;
	}

	public Sound getBreakBlock() {
		return breakBlock;
	}

	public Sound getPowerUp() {
		return powerUpSpawn;
	}

	public Sound getPowerUpSpawn() {
		return powerUpSpawn;
	}

	public Sound getPowerDown() {
		return powerDown;
	}

	public Sound getStomp() {
		return stomp;
	}

	public Sound getMarioDie() {
		return marioDie;
	}

	public Sound getMarioFinish() {
		return marioFinish;
	}

	public void dispose() {
		manager.dispose();
	}

}
