package cat.inspedralbes.m8.mariobros.tools;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.sprites.Enemy;
import cat.inspedralbes.m8.mariobros.sprites.InteractiveTileObject;
import cat.inspedralbes.m8.mariobros.sprites.Item;
import cat.inspedralbes.m8.mariobros.sprites.Mario;

public class WorldContactListener implements ContactListener {

	@Override
	public void beginContact(Contact contact) {
		Fixture fixA = contact.getFixtureA();
		Fixture fixB = contact.getFixtureB();

		int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

		switch (cDef) {

		case Constants.MARIO_HEAD_BIT | Constants.BRICK_BIT:
		case Constants.MARIO_HEAD_BIT | Constants.COIN_BIT:
			if (fixA.getFilterData().categoryBits == Constants.MARIO_HEAD_BIT)
				((InteractiveTileObject) fixB.getUserData()).onHeadHit((Mario) fixA.getUserData());
			else
				((InteractiveTileObject) fixA.getUserData()).onHeadHit((Mario) fixB.getUserData());
			break;

		case Constants.ENEMY_HEAD_BIT | Constants.MARIO_BIT:
			if (fixA.getFilterData().categoryBits == Constants.ENEMY_HEAD_BIT)
				((Enemy) fixA.getUserData()).hitOnHead();
			else
				((Enemy) fixB.getUserData()).hitOnHead();
			break;

		case Constants.ENEMY_BIT | Constants.OBJECT_BIT:
			if (fixA.getFilterData().categoryBits == Constants.ENEMY_BIT)
				((Enemy) fixA.getUserData()).recerseVelocity(true, false);
			else
				((Enemy) fixB.getUserData()).recerseVelocity(true, false);
			break;

//		case Constants.MARIO_BIT | Constants.ENEMY_BIT:
//			if (fixA.getFilterData().categoryBits == Constants.MARIO_BIT)
//				((Mario) fixA.getUserData()).hit();
//			else
//				((Mario) fixB.getUserData()).hit();
//			break;

		case Constants.ENEMY_BIT | Constants.ENEMY_BIT:
			((Enemy) fixA.getUserData()).recerseVelocity(true, false);
			((Enemy) fixB.getUserData()).recerseVelocity(true, false);
			break;

		case Constants.ITEM_BIT | Constants.OBJECT_BIT:
			if (fixA.getFilterData().categoryBits == Constants.ITEM_BIT)
				((Item) fixA.getUserData()).recerseVelocity(true, false);
			else
				((Item) fixB.getUserData()).recerseVelocity(true, false);
			break;

		case Constants.ITEM_BIT | Constants.MARIO_BIT:
			if (fixA.getFilterData().categoryBits == Constants.ITEM_BIT)
				((Item) fixA.getUserData()).useItem((Mario) fixB.getUserData());
			else
				((Item) fixB.getUserData()).useItem((Mario) fixA.getUserData());
			break;
			
		case Constants.MARIO_BIT | Constants.FLAG_BIT:
			if (fixA.getFilterData().categoryBits == Constants.MARIO_BIT)
				((Mario) fixA.getUserData()).gameWin();
			else
				((Mario) fixB.getUserData()).gameWin();
			break;
		default:
			break;
		}

	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

}
