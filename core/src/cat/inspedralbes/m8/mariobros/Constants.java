package cat.inspedralbes.m8.mariobros;

public class Constants {
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 208;
	
	public static final float VELOCITY_MARIO = 200;
	public static final float VELOCITY_MARIO_JUMP = 3.5f;
	public static final float VELOCITY_ENEMIES = 8.0f;
	
	public static final float PPM = 100;
	
	public static final short NOTHING_BIT = 0;
	public static final short GROUND_BIT = 1;
	public static final short MARIO_BIT = 2;
	public static final short BRICK_BIT = 4;
	public static final short COIN_BIT = 8;
	public static final short DESTROYED_BIT = 16;
	public static final short OBJECT_BIT = 32;
	public static final short ENEMY_BIT = 64;
	public static final short ENEMY_HEAD_BIT = 128;
	public static final short ITEM_BIT = 256;
	public static final short MARIO_HEAD_BIT = 512;
	public static final short FLAG_BIT = 1024;
	
	public static final int BLANK_COIN = 28;

}
