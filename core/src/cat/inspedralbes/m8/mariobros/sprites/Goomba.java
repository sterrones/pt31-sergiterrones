package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.scenes.Hud;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public class Goomba extends Enemy {
	private float stateTime;
	private Animation walkAnimation;
	private Array<TextureRegion> frames;
	private boolean setToDestroy;
	private boolean destroyed;
	private Assets assets;

	public Goomba(PlayScreen screen, float x, float y, Assets assets) {
		super(screen, x, y);

		this.assets = assets;

		frames = new Array<TextureRegion>();

		for (int i = 0; i < 2; i++) {
			frames.add(new TextureRegion(screen.getAtlas().findRegion("goomba"), i * 16, 0, 16, 16));
		}

		walkAnimation = new Animation(0.2f, frames);
		stateTime = 0;
		setBounds(getX(), getY(), 16 / Constants.PPM, 16 / Constants.PPM);
		setToDestroy = false;
		destroyed = false;
	}

	public void update(float deltaTime) {
		stateTime += deltaTime;
		if (setToDestroy && !destroyed) {
			world.destroyBody(b2body);
			destroyed = true;
			setRegion(new TextureRegion(screen.getAtlas().findRegion("goomba"), 32, 0, 16, 16));
			stateTime = 0;
			assets.getStomp().play();

		} else if (!destroyed) {
			b2body.setLinearVelocity(velocity);

			setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);
			setRegion((TextureRegion) walkAnimation.getKeyFrame(stateTime, true));

		}

	}

	@Override
	protected void defineEnemy() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(getX(), getY());
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);

		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / Constants.PPM);
		fdef.filter.categoryBits = Constants.ENEMY_BIT;
		fdef.filter.maskBits = Constants.GROUND_BIT | Constants.COIN_BIT | Constants.BRICK_BIT | Constants.ENEMY_BIT
				| Constants.OBJECT_BIT | Constants.MARIO_BIT;

		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);

		// Cabeza
		PolygonShape head = new PolygonShape();
		Vector2[] vertice = new Vector2[4];
		vertice[0] = new Vector2(-5, 13).scl(1 / Constants.PPM);
		vertice[1] = new Vector2(5, 13).scl(1 / Constants.PPM);
		vertice[2] = new Vector2(-5, 5).scl(1 / Constants.PPM);
		vertice[3] = new Vector2(5, 5).scl(1 / Constants.PPM);

		head.set(vertice);

		fdef.shape = head;
		fdef.restitution = 0.5f;
		fdef.filter.categoryBits = Constants.ENEMY_HEAD_BIT;
		b2body.createFixture(fdef).setUserData(this);

	}

	public void draw(Batch batch) {
		if (!destroyed || stateTime < 1)
			super.draw(batch);
	}

	@Override
	public void hitOnHead() {
		setToDestroy = true;
		Hud.addScore(1000);
	}

}
