package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.scenes.Hud;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public class Mario extends Sprite {
	public World world;
	public Body b2body;

	public enum State {
		FALLING, JUMPING, STANDING, RUNNING, GROWING, DEAD, WIN
	};

	public State currentState;
	public State previousState;

	private Animation marioRun;
	private TextureRegion marioJump;
	private TextureRegion marioStand;

	private TextureRegion bigMarioStand;
	private TextureRegion bigMarioJump;
	private Animation bigMarioRun;
	private Animation growMario;

	private boolean runningRight;
	private float stateTimer;
	public boolean marioIsBig;
	private boolean runGrowAnimation;
	private boolean timeToDefineBigMario;
	private boolean timeToRedefineMario;

	private TextureRegion marioDead;
	private boolean marioIsDead;

	boolean marioWin;

	private Assets assets;

	private Screen screen;

	public Mario(PlayScreen screen, Assets assets) {
		this.screen = screen;
		this.world = screen.getWorld();
		this.assets = assets;

		currentState = State.STANDING;
		previousState = State.STANDING;
		stateTimer = 0;
		runningRight = true;

		Array<TextureRegion> frames = new Array<TextureRegion>();

		for (int i = 1; i < 4; i++)
			frames.add(new TextureRegion(screen.getAtlas().findRegion("little_mario"), i * 16, 0, 16, 16));
		marioRun = new Animation(0.1f, frames);

		frames.clear();

		for (int i = 1; i < 4; i++)
			frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), i * 16, 0, 16, 32));
		bigMarioRun = new Animation(0.1f, frames);

		frames.clear();

		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 240, 0, 16, 32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 240, 0, 16, 32));
		frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32));

		growMario = new Animation(0.2f, frames);

		marioJump = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 80, 0, 16, 16);
		bigMarioJump = new TextureRegion(screen.getAtlas().findRegion("big_mario"), 80, 0, 16, 32);

		marioStand = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 0, 0, 16, 16);
		bigMarioStand = new TextureRegion(screen.getAtlas().findRegion("big_mario"), 0, 0, 16, 32);

		marioDead = new TextureRegion(screen.getAtlas().findRegion("little_mario"), 96, 0, 16, 16);

		defineMario();
		setBounds(0, 0, 16 / Constants.PPM, 16 / Constants.PPM);
		setRegion(marioStand);

		marioWin = false;
	}

	public float getStateTimer() {
		return stateTimer;
	}

	public void update(float deltaTime) {
		if (marioIsBig)
			setPosition(b2body.getPosition().x - getWidth() / 2,
					b2body.getPosition().y - getHeight() / 2 - 6 / Constants.PPM);

		else
			setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);

		setRegion(getFrame(deltaTime));

		if (b2body.getPosition().y <= 0 && !marioIsDead)
			hit();

		if (timeToDefineBigMario)
			defineBigMario();
		if (timeToRedefineMario)
			redefineMario();
	}

	public TextureRegion getFrame(float deltaTime) {
		currentState = getState();

		TextureRegion region;
		switch (currentState) {
//		case DEAD:
//			region = marioDead;
//			break;
//
//		case GROWING:
//			region = (TextureRegion) growMario.getKeyFrame(stateTimer);
//			if (growMario.isAnimationFinished(stateTimer))
//				runGrowAnimation = false;
//			break;
//		case JUMPING:
//			region = marioIsBig ? bigMarioJump : marioJump;
//			break;
//		case WIN:
//		case RUNNING:
//			region = (TextureRegion) (marioIsBig ? bigMarioRun.getKeyFrame(stateTimer, true)
//					: marioRun.getKeyFrame(stateTimer, true));
//			break;
//
//		case FALLING:
		case WIN:
		case DEAD:
		case STANDING:
		default:
			region = marioIsBig ? bigMarioStand : marioStand;
			break;
		}

//		if ((b2body.getLinearVelocity().x < 0 || !runningRight) && !region.isFlipX()) {
//			region.flip(true, false);
//			runningRight = false;
//		} else if ((b2body.getLinearVelocity().x > 0 || runningRight) && region.isFlipX()) {
//			region.flip(true, false);
//			runningRight = true;
//		}

		stateTimer = currentState == previousState ? stateTimer + deltaTime : 0;
		previousState = currentState;

		return region;

	}

	public State getState() {

		if (marioIsDead)
			return State.DEAD;

		if (marioWin)
			return State.WIN;

//		else if (runGrowAnimation)
//			return State.GROWING;
//
//		else if (b2body.getLinearVelocity().y > 0)
//			return State.JUMPING;
//
//		else if (b2body.getLinearVelocity().y < 0)
//			return State.FALLING;
//
//		else if (b2body.getLinearVelocity().x != 0)
//			return State.RUNNING;
//
//		else
			return State.STANDING;

	}

	public void grow() {
		runGrowAnimation = true;
		marioIsBig = true;
		timeToDefineBigMario = true;
		setBounds(getX(), getY(), getWidth(), getHeight() * 2);
		assets.getPowerUp().play();
	}

	public void defineMario() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(32 / Constants.PPM, 32 / Constants.PPM);
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);

		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / Constants.PPM);
		fdef.filter.categoryBits = Constants.MARIO_BIT;
		fdef.filter.maskBits = Constants.GROUND_BIT | Constants.BRICK_BIT | Constants.COIN_BIT | Constants.ENEMY_BIT
				| Constants.OBJECT_BIT | Constants.ENEMY_HEAD_BIT | Constants.ITEM_BIT | Constants.FLAG_BIT;

		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);

		EdgeShape head = new EdgeShape();
		head.set(new Vector2(-2 / Constants.PPM, 6 / Constants.PPM), new Vector2(2 / Constants.PPM, 6 / Constants.PPM));
		fdef.filter.categoryBits = Constants.MARIO_HEAD_BIT;
		fdef.shape = head;
		fdef.isSensor = true;

		b2body.createFixture(fdef).setUserData(this);

	}

	private void defineBigMario() {

		Vector2 currentPosition = b2body.getPosition();
		world.destroyBody(b2body);

		BodyDef bdef = new BodyDef();
		bdef.position.set(currentPosition.add(0, 10 / Constants.PPM));
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);

		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / Constants.PPM);
		fdef.filter.categoryBits = Constants.MARIO_BIT;
		fdef.filter.maskBits = Constants.GROUND_BIT | Constants.BRICK_BIT | Constants.COIN_BIT | Constants.ENEMY_BIT
				| Constants.OBJECT_BIT | Constants.ENEMY_HEAD_BIT | Constants.ITEM_BIT | Constants.FLAG_BIT;

		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);
		shape.setPosition(new Vector2(0, -14 / Constants.PPM));
		b2body.createFixture(fdef).setUserData(this);

		EdgeShape head = new EdgeShape();
		head.set(new Vector2(-2 / Constants.PPM, 6 / Constants.PPM), new Vector2(2 / Constants.PPM, 6 / Constants.PPM));
		fdef.filter.categoryBits = Constants.MARIO_HEAD_BIT;
		fdef.shape = head;
		fdef.isSensor = true;

		b2body.createFixture(fdef).setUserData(this);
		timeToDefineBigMario = false;
	}

	private void redefineMario() {
		Vector2 currentPosition = b2body.getPosition();
		world.destroyBody(b2body);

		BodyDef bdef = new BodyDef();
		bdef.position.set(currentPosition);
		bdef.type = BodyDef.BodyType.DynamicBody;
		b2body = world.createBody(bdef);

		FixtureDef fdef = new FixtureDef();
		CircleShape shape = new CircleShape();
		shape.setRadius(6 / Constants.PPM);
		fdef.filter.categoryBits = Constants.MARIO_BIT;
		fdef.filter.maskBits = Constants.GROUND_BIT | Constants.BRICK_BIT | Constants.COIN_BIT | Constants.ENEMY_BIT
				| Constants.OBJECT_BIT | Constants.ENEMY_HEAD_BIT | Constants.ITEM_BIT | Constants.FLAG_BIT;

		fdef.shape = shape;
		b2body.createFixture(fdef).setUserData(this);

		EdgeShape head = new EdgeShape();
		head.set(new Vector2(-2 / Constants.PPM, 6 / Constants.PPM), new Vector2(2 / Constants.PPM, 6 / Constants.PPM));
		fdef.filter.categoryBits = Constants.MARIO_HEAD_BIT;
		fdef.shape = head;
		fdef.isSensor = true;

		b2body.createFixture(fdef).setUserData(this);

		timeToRedefineMario = false;
	}

	public void hit() {
		if (marioIsBig) {
			marioIsBig = false;
			timeToRedefineMario = true;
			setBounds(getX(), getY(), getWidth(), getHeight() / 2);
			assets.getPowerDown().play();
		} else {
			assets.getMarioBackgroundSound().stop();
			assets.getMarioDie().play();

			marioIsDead = true;

			Filter filter = new Filter();
			filter.maskBits = Constants.NOTHING_BIT;

			for (Fixture fixture : b2body.getFixtureList())
				fixture.setFilterData(filter);
			b2body.applyLinearImpulse(new Vector2(0, 5f), b2body.getWorldCenter(), true);
		}
	}

	public void gameWin() {
		if (!marioWin) {
			assets.getMarioFinish().play();
			Hud.addScore(5000);
		}
			

		assets.getMarioBackgroundSound().stop();

		marioWin = true;
	}

}
