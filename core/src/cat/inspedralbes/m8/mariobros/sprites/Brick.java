package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.maps.MapObject;
import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.scenes.Hud;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public class Brick extends InteractiveTileObject {

	public Brick(PlayScreen screen, MapObject object, Assets assets) {
		super(screen, object, assets);
		fixture.setUserData(this);
		setCategoryFilter(Constants.BRICK_BIT);
	}

	@Override
	public void onHeadHit(Mario mario) {
		if (mario.marioIsBig) {
			setCategoryFilter(Constants.DESTROYED_BIT);
			getCell().setTile(null);
			Hud.addScore(200);
			assets.getBreakBlock().play();
		} else
			assets.getBump().play();
	}

}