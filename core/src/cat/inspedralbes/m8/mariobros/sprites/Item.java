package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;

public abstract class Item extends Sprite {
	protected PlayScreen screen;
	protected World world;
	protected Vector2 velocity;
	protected boolean toDestroy;
	protected boolean destroyed;
	protected Body body;

	public Item(PlayScreen screen, float x, float y) {
		this.screen = screen;
		this.world = screen.getWorld();
		setPosition(x, y);
		setBounds(getX(), getY(), 16 / Constants.PPM, 16 / Constants.PPM);
		defineItem();
		toDestroy = false;
		destroyed = false;
	}

	public abstract void defineItem();

	public abstract void useItem(Mario mario);

	public void draw(Batch batch) {
		if (!destroyed)
			super.draw(batch);
	}

	public void update(float deltaTime) {
		if (toDestroy && !destroyed) {
			world.destroyBody(body);
			destroyed = true;
		}
	}

	public void destroy() {
		toDestroy = true;
	}

	public void recerseVelocity(boolean x, boolean y) {
		if (x)
			velocity.x = -velocity.x;
		if (y)
			velocity.y = -velocity.y;
	}

}
