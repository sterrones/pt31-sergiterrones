package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public abstract class InteractiveTileObject {
	protected World world;
	protected TiledMap map;
	protected TiledMapTile tile;
	protected Rectangle bounds;
	protected Body body;
	protected PlayScreen screen;
	protected MapObject object;

	protected Fixture fixture;

	Assets assets;

	public InteractiveTileObject(PlayScreen screen, MapObject object, Assets assets) {
		this.world = screen.getWorld();
		this.map = screen.getMap();
		this.object = object;
		this.bounds = ((RectangleMapObject) object).getRectangle();
		this.assets = assets;
		this.screen = screen;

		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();

		bdef.type = BodyDef.BodyType.StaticBody;
		bdef.position.set((bounds.getX() + bounds.getWidth() / 2) / Constants.PPM,
				(bounds.getY() + bounds.getHeight() / 2) / Constants.PPM);

		body = world.createBody(bdef);

		shape.setAsBox(bounds.getWidth() / 2 / Constants.PPM, bounds.getHeight() / 2 / Constants.PPM);
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
	}

	public abstract void onHeadHit(Mario mario);

	public void setCategoryFilter(short filterBit) {
		Filter filter = new Filter();
		filter.categoryBits = filterBit;
		fixture.setFilterData(filter);
	}

	public TiledMapTileLayer.Cell getCell() {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
		return layer.getCell((int) (body.getPosition().x * Constants.PPM / 16),
				(int) (body.getPosition().y * Constants.PPM / 16));
	}

}
