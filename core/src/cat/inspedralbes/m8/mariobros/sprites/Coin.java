package cat.inspedralbes.m8.mariobros.sprites;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Vector2;

import cat.inspedralbes.m8.mariobros.Constants;
import cat.inspedralbes.m8.mariobros.scenes.Hud;
import cat.inspedralbes.m8.mariobros.screens.PlayScreen;
import cat.inspedralbes.m8.mariobros.tools.Assets;

public class Coin extends InteractiveTileObject {

	private static TiledMapTileSet tileSet;

	public Coin(PlayScreen screen, MapObject object, Assets assets) {
		super(screen, object, assets);

		tileSet = map.getTileSets().getTileSet("NES - Super Mario");

		fixture.setUserData(this);
		setCategoryFilter(Constants.COIN_BIT);
	}

	@Override
	public void onHeadHit(Mario mario) {
		if (getCell().getTile().getId() == Constants.BLANK_COIN)
			assets.getBump().play();

		else {
			if (object.getProperties().containsKey("mushroom")) {
				assets.getPowerUp().play();
				screen.spawnItem(new ItemDef(
						new Vector2(body.getPosition().x, body.getPosition().y + 16 / Constants.PPM), Mushroom.class));
			} else {
				assets.getCoin().play();

			}

			getCell().setTile(tileSet.getTile(Constants.BLANK_COIN));
			Hud.addScore(500);

		}

	}

}
